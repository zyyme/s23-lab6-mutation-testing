from bonus_system import calculateBonuses
import random
import string

def test_standard_program_small_amount():
    amount = random.randint(0, 10000)
    
    assert calculateBonuses('Standard', amount) == 1 * 0.5

def test_standard_program_exact_medium_amount():
    amount = 10000

    assert calculateBonuses('Standard', amount) == 1.5 * 0.5

def test_standard_program_medium_amount():
    amount = random.randint(10000, 50000)
    
    assert calculateBonuses('Standard', amount) == 1.5 * 0.5

def test_standard_program_exact_large_amount():
    amount = 50000

    assert calculateBonuses('Standard', amount) == 2 * 0.5

def test_standard_program_large_amount():
    amount = random.randint(50000, 100000)
    
    assert calculateBonuses('Standard', amount) == 2 * 0.5

def test_standard_program_exact_big_amount():
    amount = 100000

    assert calculateBonuses('Standard', amount) == 2.5 * 0.5

def test_standard_program_biggest_amount():
    amount = random.randint(100000, 1000000)
    
    assert calculateBonuses('Standard', amount) == 2.5 * 0.5


def test_premium_program_small_amount():
    amount = random.randint(0, 10000)
    
    assert calculateBonuses('Premium', amount) == 1 * 0.1

def test_premium_program_medium_amount():
    amount = random.randint(10000, 50000)
    
    assert calculateBonuses('Premium', amount) == 1.5 * 0.1

def test_premium_program_large_amount():
    amount = random.randint(50000, 100000)
    
    assert calculateBonuses('Premium', amount) == 2 * 0.1

def test_premium_program_biggest_amount():
    amount = random.randint(100000, 1000000)
    
    assert calculateBonuses('Premium', amount) == 2.5 * 0.1

def test_diamond_program_small_amount():
    amount = random.randint(0, 10000)
    
    assert calculateBonuses('Diamond', amount) == 1 * 0.2

def test_diamond_program_medium_amount():
    amount = random.randint(10000, 50000)
    
    assert calculateBonuses('Diamond', amount) == 1.5 * 0.2

def test_diamond_program_large_amount():
    amount = random.randint(50000, 100000)
    
    assert calculateBonuses('Diamond', amount) == 2 * 0.2

def test_diamond_program_biggest_amount():
    amount = random.randint(100000, 1000000)
    
    assert calculateBonuses('Diamond', amount) == 2.5 * 0.2

def test_random_program_small_amount():
    program_lte = 'B'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    program_gte = 'E'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    amount = random.randint(0, 10000)
    
    assert calculateBonuses(program_lte, amount) == 0
    assert calculateBonuses(program_gte, amount) == 0

def test_random_program_medium_amount():
    program_lte = 'B'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    program_gte = 'E'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    amount = random.randint(10000, 50000)
    
    assert calculateBonuses(program_lte, amount) == 0
    assert calculateBonuses(program_gte, amount) == 0

def test_random_program_large_amount():
    program_lte = 'B'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    program_gte = 'E'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    amount = random.randint(50000, 100000)
    
    assert calculateBonuses(program_lte, amount) == 0
    assert calculateBonuses(program_gte, amount) == 0

def test_random_program_biggest_amount():
    program_lte = 'B'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    program_gte = 'E'.join(random.sample((string.ascii_lowercase + string.ascii_uppercase), 10))
    amount = random.randint(100000, 1000000)
    
    assert calculateBonuses(program_lte, amount) == 0
    assert calculateBonuses(program_gte, amount) == 0